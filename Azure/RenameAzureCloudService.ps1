import-module Azure

# download publish settings
Get-AzurePublishSettingsFile

# import settings you just downloaded
Import-AzurePublishSettingsFile "C:\Users\<user>\Downloads\MySub-DATE-credentials.publishsettings"

# view subscription details
Get-AzureSubscription

# select active subscription
Select-AzureSubscription 'MySub'

# view cloud services for selected sub
Get-AzureService 

# update cloud service name/description
Set-AzureService 'myServiceName' 'Friendly Service Name' 'Detailed Service Description'