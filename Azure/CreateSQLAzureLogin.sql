--connect to master DB
CREATE LOGIN reports WITH password='password';
CREATE username reports FROM LOGIN reports;

--connect to use DB
CREATE username reports FROM LOGIN reports;
EXEC sp_addrolemember 'db_datareader', 'reports';