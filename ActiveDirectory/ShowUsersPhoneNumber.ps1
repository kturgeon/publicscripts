# Display phone number values for all user accounts
# Many organizations use Active Directory as a telephone directory. But there are usually some 
# phone numbers that are unaccounted for. The following script will show the phone number value for 
# the usernames of all user accounts:
Get-AdUser -Filter * -Properties OfficePhone | FT OfficePhone,UserPrincipalName
