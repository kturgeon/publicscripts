# The following PowerShell script will show user accounts with the password set to not expire, 
# sorted by the user name, object class (user, computer, etc.), and UPN fields:
Search-ADAccount -PasswordNeverExpires | FT Name,  ObjectClass, UserPrincipalName
