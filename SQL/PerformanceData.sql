SELECT 

(total_elapsed_time/execution_count)/1000 AS [Avg Exec Time in ms]

, max_elapsed_time/1000 AS [MaxExecTime in ms]

, min_elapsed_time/1000 AS [MinExecTime in ms]

, (total_worker_time/execution_count)/1000 AS [Avg CPU Time in ms]

, qs.execution_count AS NumberOfExecs

, (total_logical_writes+total_logical_Reads)/execution_count AS [Avg Logical IOs]

, max_logical_reads AS MaxLogicalReads

, min_logical_reads AS MinLogicalReads

, max_logical_writes AS MaxLogicalWrites

, min_logical_writes AS MinLogicalWrites

,(

SELECT SUBSTRING(text,statement_start_offset/2,

(

CASE WHEN statement_end_offset = -1 

then LEN(CONVERT(nvarchar(max), text)) * 2 

ELSE statement_end_offset 

end -statement_start_offset)/2

)

FROM sys.dm_exec_sql_text(sql_handle)

) AS query_text

FROM sys.dm_exec_query_stats qs

ORDER BY [Avg Exec Time in ms] DESC
