SET NOCOUNT ON
SET ROWCOUNT 1
WHILE 1 = 1
 BEGIN
   DELETE  
   FROM Customers
   WHERE contactname IN
        (SELECT  contactname
         FROM    Customers
         GROUP BY contactname
         HAVING  COUNT(*) > 1)
      IF @@Rowcount = 0
      BREAK ;
 END
 SET ROWCOUNT 0