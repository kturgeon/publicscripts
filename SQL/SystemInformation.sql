--BOTH ON PREMISE AND AZURE
SELECT @@ServerName as SQLServerName, 
       (SELECT CONVERT(char(100), SERVERPROPERTY('MachineName')))  as MachineName, (SELECT CONVERT(char(50), SERVERPROPERTY('InstanceName'))) as InstanceName,
	   (SELECT CONVERT(char(30), SERVERPROPERTY('EDITION'))) as Edition, (SELECT CONVERT(char(30), SERVERPROPERTY('ProductLevel'))) as ProductLevel,
	   (SELECT CONVERT(char(30), SERVERPROPERTY('ProductVersion'))) as ProductVersion,
	   CONVERT(NVARCHAR(10), (SELECT VALUE FROM SYS.CONFIGURATIONS where Name like 'max server memory%')) as MaxMemoryMB,
	   CONVERT(NVARCHAR(10), (SELECT VALUE FROM SYS.CONFIGURATIONS where Name like 'min server memory%')) as MinMemoryMB,
	   (SELECT DEFAULT_DOMAIN()) as DomainName,
	   (SELECT CONVERT(char(30), SERVERPROPERTY('ISClustered'))) as IsClustered,
	   (SELECT CONVERT(char(30), SERVERPROPERTY('ISIntegratedSecurityOnly')))  as IsIntegratedSecurityOnly,
	   (SELECT CONVERT(char(30), SERVERPROPERTY('ISSingleUser'))) as IsSingleUser,
	   (SELECT CONVERT(char(30), SERVERPROPERTY('COLLATION'))) as Collation,
	   (SELECT REPLACE(CAST(SERVERPROPERTY('ErrorLogFileName') AS VARCHAR(500)), 'ERRORLOG','')) as ErrorLogLocation,
	   (SELECT VALUE FROM SYS.CONFIGURATIONS where Name like 'default trace%') as IsTraceEnabled,
	   (SELECT REPLACE(CONVERT(VARCHAR(100),SERVERPROPERTY('ErrorLogFileName')), '\ERRORLOG','\log.trc')) as TraceFileLocation,
	   (SELECT VALUE FROM SYS.CONFIGURATIONS where Name like 'filestream access%') as IsFileStreamsEnabled,
	   (SELECT VALUE FROM SYS.CONFIGURATIONS where Name like 'backup compression%') as IsBackupCompressionEnabled

--ON PREMISE ONLY
SELECT (SELECT  createdate FROM sys.syslogins where sid = 0x010100000000000512000000) as InstallDate,	   
	   (SELECT cpu_count FROM sys.dm_os_sys_info) as LogicalCPUCount,
       (CONVERT(NVARCHAR(10),(select physical_memory_kb from sys.dm_os_sys_info))/1024) as TotalMemoryMB,
       (SELECT top 1 service_account FROM sys.dm_server_services) as ServiceAccount,
	   (SELECT COUNT(*) FROM sys.servers WHERE is_linked ='1') as LinkedServerCount,
	   (SELECT COUNT(*) FROM sys.server_principals WHERE IS_SRVROLEMEMBER('sysadmin', name) = 1) as SysAdminCount
