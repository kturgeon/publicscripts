set PDI=C:\"Program Files"\Pentaho\data-integration

md %PDI%\backup

md %PDI%\backup\classes
move %PDI%\classes\kettle-lifecycle-listeners.xml %PDI%\backup\classes
move %PDI%\classes\kettle-registry-extensions.xml %PDI%\backup\classes

md %PDI%\backup\lib
move %PDI%\lib\pdi-engine-api-*.jar %PDI%\backup\lib
move %PDI%\lib\pdi-engine-spark-*.jar %PDI%\backup\lib
move %PDI%\lib\pdi-osgi-bridge-core-*.jar %PDI%\backup\lib
move %PDI%\lib\pdi-spark-driver-*.jar %PDI%\backup\lib
move %PDI%\lib\pentaho-connections-*.jar %PDI%\backup\lib
move %PDI%\lib\pentaho-cwm-*.jar %PDI%\backup\lib
move %PDI%\lib\pentaho-hadoop-shims-api-*.jar %PDI%\backup\lib
move %PDI%\lib\pentaho-osgi-utils-api-*.jar %PDI%\backup\lib

md %PDI%\backup\plugins
move %PDI%\plugins\kettle5-log4j-plugin %PDI%\backup\plugins
move %PDI%\plugins\pdi-xml-plugin %PDI%\backup\plugins
move %PDI%\plugins\pentaho-big-data-plugin %PDI%\backup\plugins

md %PDI%\backup\system
move %PDI%\system\karaf %PDI%\backup\system
move %PDI%\system\mondrian %PDI%\backup\system
move %PDI%\system\osgi %PDI%\backup\system
