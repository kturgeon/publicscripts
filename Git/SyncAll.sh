#!/bin/bash
#
# Pull this folder in C:\Program Files\Git
# Execute this when you opena Git Bash window by running:
#   $ ./PullAllRepositories.sh  
#
for dir in /c/repos/*/
do
    dir=${dir%*/}
    echo ${dir##*/}
	cd /c/repos/${dir##*/}
	git pull
done